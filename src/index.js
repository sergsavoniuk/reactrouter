import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, MemoryRouter } from 'react-router-dom'
import App from './App'

ReactDOM.render((
  <MemoryRouter>
    <App />
  </MemoryRouter>
), document.getElementById('root'));