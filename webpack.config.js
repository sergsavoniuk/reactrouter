var path = require('path');
var webpack = require('webpack');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: "./src/index.js",

  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },

  plugins: [
    new ExtractTextPlugin('bundle.css'),
  ],

  resolve: {
    modules: ['node_modules']
  },

  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [path.resolve(__dirname, 'src')]
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
      }
    ]
  },

  devServer: {
    historyApiFallback: true,
    port: 3000,
    hot: true,
    contentBase: path.resolve(__dirname, 'public'),
    publicPath: '/',
    proxy: [
      {
        context: ['/api/**'],
        target: "http://localhost:8080",
        ws: true,
        secure: false
      }
    ]
  }
}